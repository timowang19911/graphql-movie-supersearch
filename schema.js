const axios = require('axios');
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull
} = require('graphql')

function sortByCountAndOrder(list) {
    const listMap = {};
    const listCount = {};
    list.forEach((item) => {
        listMap[item.id] = item;
        listCount[item.id] = listCount[item.id] ? listCount[item.id] + 1 : 1;
    });
    const sortedIds = Object.keys(listCount).sort((a, b) =>
        listCount[b] * (list.length - listMap[b].order) - listCount[a] * (list.length - listMap[a].order)
    );
    const sortedData = sortedIds.map((id) => listMap[id]);
    return sortedData;
}

const QueryResultType = new GraphQLObjectType({
    name: 'QueryResultType',
    fields: {
        id: {
            type: GraphQLInt
        },
        title: {
            type: GraphQLString,
            resolve: qResult => qResult.title || qResult.name
        },
        overview: {
            type: GraphQLString
        },
        media_type: {
            type: GraphQLString
        }
    }
})

const CastResultType = new GraphQLObjectType({
    name: 'CastResultType',
    fields: {
        id: {
            type: GraphQLInt
        },
        character: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString
        }
    }
})

const SearchType = new GraphQLObjectType({
    name: 'SearchType',
    fields: {
        queryResults: {
            type: new GraphQLList(QueryResultType),
            resolve: qResults => qResults.slice(0, 5)
        },
        creditResults: {
            type: new GraphQLList(CastResultType),
            resolve: qResults => Promise.all(
                qResults
                    .sort((a, b) => b.vote_count - a.vote_count)
                    .slice(0, 5)
                    .filter((item) => item.media_type === 'movie' || item.media_type === 'tv')
                    .map((item) => axios.get(`https://api.themoviedb.org/3/${item.media_type}/${item.id}/credits?api_key=88bffaba66bb9564b33730c9afb7b61f`))
            ).then(results => {
                const concatData = results.reduce((concatArr, item) => concatArr.concat(item.data.cast.slice(0, 15)), []);
                // console.log('====== concatData', concatData);
                // console.log('=======', qResults.sort((a, b) => b.vote_count - a.vote_count));
                // console.log('======= sortedData', sortByCountAndOrder(concatData));
                return sortByCountAndOrder(concatData);
            })
        }
    }
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        multisearch: {
            type: SearchType,
            args: {
                query: {
                    type: GraphQLString
                }
            },
            resolve: (root, args) =>
                axios.get(`https://api.themoviedb.org/3/search/multi?api_key=88bffaba66bb9564b33730c9afb7b61f&query=${encodeURI(args.query)}`)
                    .then(res => res.data.results)
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery,
});
