## How to run

```
$ npm install
$ npm run dev   # for dev, use 'npm run start' for production
```

open browser and enter:
```
http://localhost:4000/graphql
```